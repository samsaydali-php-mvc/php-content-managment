<?php
  /*
   * Base Controller
   * Loads the models and views
   */
  class Controller {
    // Load model
    public function model($model){
      // Require model file
      require_once '../app/models/' . $model . '.php';

      // Instatiate model
      return new $model();
    }

    // Load view
    public function view($view, $data = []){
      // Check for view file
      if(file_exists('../app/views/' . $view . '.php')){
        require_once '../app/views/' . $view . '.php';
      } else {
        // View does not exist
        die('View does not exist');
      }
    }

    protected function method($method){
        if($_SERVER['REQUEST_METHOD'] != $method){
            throw new Exception('Wrong Method');
        }
    }

    protected function authenticate(){

      $token = null;
      $headers = apache_request_headers();

      if(isset($headers['Authorization'])){
          $token = $headers['Authorization'];
      }

      $token = JWTHelper::valid($token);

      return $token;
    }
  }