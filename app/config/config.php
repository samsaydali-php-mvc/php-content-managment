<?php
  // DB Params
  define('DB_HOST', 'localhost');
  define('DB_USER', 'root');
  define('DB_PASS', '');
  define('DB_NAME', 'mvcplayground');

  // App Root
  define('APPROOT', dirname(dirname(__FILE__)));
  // URL Root like: http://localhost/mvcplayground
  define('URLROOT', 'http://localhost/mvcplayground');
  // Site Name
  define('SITENAME', 'PlayGround');

  // What Origins Can Access
  define('ALLOWED_ACCESS','*');

  // Auth Key
  define('AUTH_KEY','MySecretKey');