<?php
/**
 * Created by PhpStorm.
 * User: Samer
 * Date: 9/29/2018
 * Time: 3:53 PM
 */

class RequestManager
{
    private $contentTypeManager;

    public function __construct(){
        $this->contentTypeManager = new ContentTypesManger();
    }

    public function hasType($type){
        return $this->contentTypeManager->hasType($type);
    }

    public function getTypeController($type){
        if($this->hasType($type)){
            return new CMController($type);
        }else {
            return false;
        }
    }
}