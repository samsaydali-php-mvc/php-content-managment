<?php
/**
 * Created by PhpStorm.
 * User: Samer
 * Date: 10/2/2018
 * Time: 3:18 PM
 */

class CMController extends AbstractContentController
{
    public function __construct($tableName){
        $this->mainModel = new AbstractModel($tableName);
    }
}