<?php
/**
 * Created by PhpStorm.
 * User: Samer
 * Date: 9/29/2018
 * Time: 4:34 PM
 */

class ContentTypesManger
{

    private $content_type_path = '../app/managment';
    private $content_type_file = 'content_types.json';
    private $jsonHandler;

    public function __construct(){
        $this->jsonHandler = new JSONHandler($this->content_type_path);
    }

    public function hasType($type){
        $contents = $this->jsonHandler->read($this->content_type_file);
        return array_key_exists($type,$contents);
    }
}