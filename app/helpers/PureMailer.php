<?php
/**
 * Created by PhpStorm.
 * User: PC-02
 * Date: 9/9/2018
 * Time: 8:31 PM
 */

/* TODO : Not in Bootstrap */
class PureMailer
{
    public function SendContactMail($from,$company,$service,$the_message,$mail,$phone){
        $to = EMAIL;
        $subject = "New Contact Mail";
        $message = MailTemplateHelper::getContactMailTemplate($from,$company,$service,$the_message,$mail ,$phone);

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <'.EMAIL.'>' . "\r\n";
        $headers .= 'Cc: '. EMAIL . "\r\n";

        mail($to,$subject,$message,$headers);

    }

    public function SendNewPasswordMail($newPassword,$link){
        $to = EMAIL;
        $subject = "Forgot Password Mail";
        $message = MailTemplateHelper::getNewPasswordMailTemplate($newPassword,$link);

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <'.EMAIL.'>' . "\r\n";
        $headers .= 'Cc: '. EMAIL . "\r\n";

        mail($to,$subject,$message,$headers);
    }
}