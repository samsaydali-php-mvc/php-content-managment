<?php
/**
 * Created by PhpStorm.
 * User: PC-02
 * Date: 9/3/2018
 * Time: 9:06 PM
 */

class Uploader
{

    // Create uploads folder in your public folder
    private static $path = 'uploads/';

    public static function upload($filed){

        $url = null;

        if(!empty($_FILES[$filed]))
        {
            $filename = (new DateTime())->format('Y-m-d-H-i-s'). basename( $_FILES[$filed]['name']);
            $path = Uploader::$path .$filename;
            if(move_uploaded_file($_FILES[$filed]['tmp_name'], $path)) {
                $url = URLROOT.'/'.$path;
            }
        }

        return $url;
    }
}