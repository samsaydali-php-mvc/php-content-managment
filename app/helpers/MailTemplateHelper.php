<?php
/**
 * Created by PhpStorm.
 * User: PC-02
 * Date: 9/2/2018
 * Time: 9:05 PM
 */

class MailTemplateHelper
{
    public static function getContactMailTemplate($from,$company,$service,$message,$mail,$phone){
        return '<head>
                <link href="https://fonts.googleapis.com/css?family=Open+Sans|Play|Roboto+Condensed|Ubuntu" rel="stylesheet">
                <style>
                    .main {
                        text-align: left;
                        color: rgba(241, 233, 233, 0.87);
                        max-width: 500px;
                        margin: 50px auto;
                        font-family: \'Play\', sans-serif
                    }
            
                    h2,
                    h3 {
                        font-weight: normal;
                        color: #313131;
                        padding-left: 10px;
                    }
            
                    span {
                        color: #2b3643;
                        font-weight: normal;
                    }
                </style>
            </head>
            
            <body>
                <div class="main">
                    <h1 style="background:#313131;padding:10px">You Got A Contact Request.</h1>
                    <br>
                    <h2>From: '.$from.'</h2>
                    <h2>Company: '.$company.'</h2>
                    <h2>Service: '.$service.'</h2>
            
                    <br>
                    <h2>Message:</h2>
                    <h3>'.$message.'</h3>
            
                    <br>
            
                    <h2>Contact Info:</h2>
                    <h3>E-Mail:
                        <span>'.$mail.'
                            <span>
                    </h3>
                    <h3>Phone: '.$phone.'</h3>
            
                    <hr>
                    <p style="text-align: center;color: #2b3643">InMood | InMood.net</p>
            
                </div>
            </body>';
    }

    public static function getNewPasswordMailTemplate($newPassword,$link){
        return '<head>
                    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Play|Roboto+Condensed|Ubuntu" rel="stylesheet">
                    <style>
                        .main {
                            text-align: left;
                            color: rgba(241, 233, 233, 0.87);
                            max-width: 500px;
                            margin: 50px auto;
                            font-family: \'Play\', sans-serif
                        }
                
                        h2,
                        h3 {
                            font-weight: normal;
                            color: #313131;
                            padding-left: 10px;
                        }
                
                        a {
                            font-weight: normal;
                            /*padding:10px;*/
                            word-wrap: break-word;
                        }
                
                        span {
                            color: #2b3643;
                            font-weight: normal;
                        }
                    </style>
                </head>
                
                <body>
                    <div class="main">
                        <h1 style="background:#313131;padding:10px">Password Reset Request.</h1>
                        <br>
                        <h2>Your new password will be: '.$newPassword.'</h2>
                        <br>
                        <h2>if you didn\'t request this, you can ignore it.</h2>
                        <br>
                        <h2>if you, confirm by clicking the link below:</h2>
                
                        <div style="padding:20px; max-width:450px">
                            <a href="${link}">'.$link.'</a>
                        </div>
                        <hr>
                        <p style="text-align: center;color: #2b3643">InMood | InMood.net</p>
                
                    </div>
                </body>';
    }
}