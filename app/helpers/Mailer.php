<?php
/**
 * Created by PhpStorm.
 * User: PC-02
 * Date: 9/2/2018
 * Time: 8:49 PM
 */


class Mailer
{
    private $transport;
    private $mailer;
    public function __construct(){
        $this->transport = new Swift_SmtpTransport(EMAIL_HOST,EMAIL_PORT);
        $this->transport->setUsername(EMAIL);
        $this->transport->setPassword(EMAIL_PASSWORD);

        $this->mailer = new Swift_Mailer($this->transport);
    }

    public function sendTestMail(){
        $message = new Swift_Message('From PHP');
        $message->setFrom([EMAIL]);
        $message->setTo([EMAIL]);
        $message->setContentType('text/html');
        $message->setBody(MailTemplateHelper::getContactMailTemplate('me','company','service','message','mail','phone'));

        $this->mailer->send($message);
    }

    /**
     * Send the contact email
     */
    public function SendContactMail($from,$company,$service,$the_message,$mail,$phone){
        $message = new Swift_Message('New Contact Mail');
        $message->setFrom([EMAIL]);
        $message->setTo([EMAIL]);
        $message->setContentType('text/html');
        $message->setBody(MailTemplateHelper::getContactMailTemplate($from,$company,$service,$the_message,$mail ,$phone));

        $this->mailer->send($message);
    }

    /**
     * Send the new password email
     */
    public function SendNewPasswordMail($newPassword,$link){
        $message = new Swift_Message('Password Change Request');
        $message->setFrom([EMAIL]);
        $message->setTo([EMAIL]);
        $message->setContentType('text/html');
        $message->setBody(MailTemplateHelper::getNewPasswordMailTemplate($newPassword,$link));

        $this->mailer->send($message);
    }

    public function setHeaders($swift_message,$from,$to,$content_type = null){
            $headers = $swift_message->getHeaders();
            $message_id = time() .'-' . md5($from . $to) . '@'.substr(strrchr($from, "@"), 1);
            $headers->addIdHeader('Message-ID', $message_id);
            $headers->addTextHeader('MIME-Version', '1.0');
            $headers->addTextHeader('X-Mailer', 'PHP v' . phpversion());
            $content_type != null ?
                $headers->addParameterizedHeader('Content-type', $content_type, ['charset' => 'utf-8']):
                $headers->addParameterizedHeader('Content-type', 'text/html', ['charset' => 'utf-8']);
    }

}