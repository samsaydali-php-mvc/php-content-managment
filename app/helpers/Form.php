<?php
/**
 * Created by PhpStorm.
 * User: Samer
 * Date: 9/28/2018
 * Time: 10:06 PM
 */

/**
 * For Building Dynamic Bootstrap 4.1 Forms
 * @author Samer Alsaydali
 * @since 9/28/2018
 * @version 1.0
*/
class Form
{
    /**
     * Opens a form with an action and a method
     * @param $action string action endpoint like: /posts/insert
     * @param $method string http method
     * @return void
     */
    public static function open($action, $method)
    {
        $action = URLROOT . $action;
        echo "<form action=$action method=$method class='my-5'>";
    }

    /**
     * Closes the form tag.
     * @return void
     */
    public static function close()
    {
        echo "</form>";
    }

    /**
     * Prints an input with given params
     * @param $name string filed name
     * @param $value string value if any
     * @param $type string filed type
     * @param $help string help text
     * @return void
     */
    public static function text($name, $type = null, $value = null, $help = null)
    {
        $type = ($type == null ? 'text' : $type);
        $value = ($value == null ? '' : $value);
        $help = ($help == null ? '' : "<small id=\"help-$name\" class=\"form - text text - muted\">$help</small>");
        $label = ucfirst($name);
        echo "<div class=\"form-group\">
                <label for=\"input-$name\">$label</label>
                <input type=$type value=\"$value\" name=$name class=\"form-control\" id=\"input-$name\" aria-describedby=\"help-$name\" placeholder=\"Enter $name\">
                
                 <small id=\"help-$name\" class=\"form-text text-muted\">$help</small>
              </div>";
    }

    /**
     * Prints a textarea with given params
     * @param $name string filed name
     * @param $value string the value if any
     * @param $rows integer number of rows
     * @return void
     */
    public static function textarea($name, $value, $rows = null)
    {
        $label = ucfirst($name);
        $rows = ($rows == null ? 3 : $rows);
        echo "<div class=\"form-group\">
                <label for=\"$name-Textarea1\">$label</label>
                <textarea placeholder=\"Enter $name \" name=$name class=\"form-control\" id=\"$name-Textarea\" rows=$rows>$value</textarea>
              </div>";
    }

    /**
     * Prints a submit button with given params
     * @param $text string text on button
     * @return void
     */
    public static function submit($text = null)
    {
        $text = ($text == null ? 'Submit' : $text);

        echo "<div class='my-3'><button type=\"submit\" class=\"btn btn-primary\">$text</button></div>";
    }

    /**
     * Prints a select menu with several value => display_name options
     * @param $name string the filed name
     * @param $options array of  value => display_name
     * @param $value string the value if any
     * @param $placeholder string the placeholder
     * @param $label string label before menu
     * @return void
     */
    public static function select($name, $options = [], $value = null, $placeholder = null, $label = null)
    {
        $label = ($label != null ? $label : ucfirst($name));
        $placeholder = ($placeholder == null) ? "<option disabled>Chose from below</option>" : "<option selected disabled>$placeholder</option>";
        $list_options = "";
        foreach ($options as $option_value => $option_name) {
            $selected = $option_value == $value ? " selected " : "";
            $list_options .= "<option value=\"$option_value\" $selected>$option_name</option>";
        }

        echo "
               <div class='my-3'>
               <label>$label</label>
               <select value=\$value\ class=\"form-control\" name=$name>
                  $placeholder
                  
                  $list_options 
              </select></div>";
    }

    /**
     * Prints a file upload option
     * @param $name string the filed name
     * @return void
     */
    public static function file($name)
    {
        $label = ucfirst($name);
        echo "  <div class=\"form-group\">
                    <label for=\"$name-File1\">$label</label>
                    <input type=\"file\" name=$name class=\"form-control-file\" id=\"$name-File1\">
                </div>";
    }

    /**
     * Prints a set of related radio buttons
     * @param $name string the filed name
     * @param $options array of  value => display_name
     * @param $value string the value if any
     * @return void
     */
    public static function radio($name, $options = [], $value = null)
    {
        $i = 1;
        $label = ucfirst($name);
        echo "<label>$label</label>";
        foreach ($options as $option_value => $display) {
            $checked = ($option_value == $value ? 'checked' : '');
            echo "<div class=\"form-check\">
                      <input class=\"form-check-input\" type=\"radio\" value=$option_value name=$name id=\"$name-Check-$i\" $checked>
                      <label class=\"form-check-label\" for=\"$name-Check-$i\">
                        $display
                      </label>
                   </div>";
            $i++;
        }
    }

    /**
     * Prints a set of related radio buttons
     * @param $name string the filed name
     * @param $checked boolean determine if checked or not
     * @param $label string custom label for the radio buttons set
     * @return void
     */
    public static function checkbox($name, $value, $checked = null, $label = null)
    {
        $label = ($label != null ? $label : ucfirst($name));
        $checked = ($checked == true ? 'checked' : '');
        echo "<div class=\"form-check form-check-inline my-3\">
                  <input class=\"form-check-input\" name=$name type=\"checkbox\" id=\"$name-Checkbox\" value=$value $checked>
                  <label class=\"form-check-label\" for=\"$name-Checkbox\">$label</label>
               </div>";
    }
}