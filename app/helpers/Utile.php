<?php
/**
 * Created by PhpStorm.
 * User: PC-02
 * Date: 9/6/2018
 * Time: 12:17 AM
 */

class Utile
{
    public static function randomStringGenerate($length = 10){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}