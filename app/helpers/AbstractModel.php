<?php
/**
 * Created by PhpStorm.
 * User: PC-02
 * Date: 8/28/2018
 * Time: 8:26 PM
 */

class AbstractModel
{
   protected $db;
   protected $table;
   protected $idFiled;

   public function __construct($table_name,$idFiled = null){
       $this->db = new Database();
       $this->table = $table_name;
       $idFiled != null ? $this->idFiled = $idFiled : $this->idFiled = 'id';
   }

    public function fetch(){
        $sql = 'SELECT * FROM '.$this->table;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function fetchById($id){
        $sql = 'SELECT * FROM '.$this->table.' WHERE '. $this->idFiled .' = :id';
        $this->db->query($sql);
        $this->db->bind(':id',$id);
        return $this->db->single();
    }

    public function getByFiled($filed,$value){
        $sql = 'SELECT * FROM '.$this->table.' WHERE '. $filed .' = :value';
        $this->db->query($sql);
        $this->db->bind(':value',$value);
        return $this->db->resultSet();
    }

    public function delete($id){
        $this->db->query('DELETE FROM '.$this->table.' WHERE '.$this->idFiled.' = :id');
        $this->db->bind(':id',$id);
        return $this->db->execute();
    }

    // Should implement insertion
    public function insert($data){
       $insertInto = 'INSERT INTO '.$this->table.' (';
       $keys = '';
       $valuesSeparator = ') VALUES (';
       $values = '';
       $last = ');';

       /* Put Keys and Values */
       $first = true;
       foreach ($data as $key => $value){
            if($first){
                $keys.= $key;
                $values.= ':'.$key;
                $first = false;
            } else {
                $keys.= ', '.$key;
                $values.= ', :'.$key;
            }
       }
       $sql = $insertInto.$keys.$valuesSeparator.$values.$last;
       $this->db->query($sql);

       // Bind The Values
       foreach ($data as $key => $value){
           $this->db->bind(':'.$key,$value);
       }
       return $this->db->execute();
    }

    // Should be overridden
    public function update($id,$data){
        $update = 'UPDATE '.$this->table;
        $set = " SET ";
        $keys_values = '';
        $where = ' WHERE '.$this->idFiled.' = :id';
        /* Put Keys and Values */
        $first = true;
        foreach ($data as $key => $value){
            if($first){
                $keys_values.= ' '.$key.' = :'.$key;
                $first = false;
            } else {
                $keys_values.= ', '.$key.' = :'.$key;
            }
        }
        $sql = $update.$set.$keys_values.$where;
        $this->db->query($sql);

        // Bind The Values
        foreach ($data as $key => $value){
            $this->db->bind(':'.$key,$value);
        }
        $this->db->bind(':id',$id);
        return $this->db->execute();
    }
}