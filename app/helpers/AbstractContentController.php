<?php

/**
 * Class AbstractContentController
 * A fully implemented CRUD methods for a content.
 * @author Samer Alsaydali
 */

abstract class AbstractContentController extends Controller
{
    protected $mainModel;

    public function index(){
        header("Access-Control-Allow-Origin: ".ALLOWED_ACCESS);
        header('Content-Type: application/json; charset=UTF-8');
        $data = $this->mainModel->fetch();
        echo (json_encode([
            'success' => true,
            'data' => $data
        ],JSON_UNESCAPED_UNICODE));
    }

    public function getById($id){
        header("Access-Control-Allow-Origin: ".ALLOWED_ACCESS);
        header('Content-Type: application/json; charset=UTF-8');
        $single = $this->mainModel->fetchById($id);
        echo (json_encode([
            'success' => true,
            'data' => $single
        ] ,JSON_UNESCAPED_UNICODE));
    }

    public function getByFiled($filed,$value){
        header("Access-Control-Allow-Origin: ".ALLOWED_ACCESS);
        header('Content-Type: application/json; charset=UTF-8');
        $single = $this->mainModel->getByFiled($filed,$value);
        echo (json_encode([
            'success' => true,
            'data' => $single
        ] ,JSON_UNESCAPED_UNICODE));
    }


    public function delete(){
        header("Access-Control-Allow-Origin: ".ALLOWED_ACCESS);
        header('Content-Type: application/json; charset=UTF-8');
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type");

        try{
            $this->method('POST');
            //$this->authenticate();
            $id = $_POST['id'];
            $this->mainModel->delete($id);
            echo (json_encode([
                'success' => true,
            ] ,JSON_UNESCAPED_UNICODE));
        } catch (Exception $e){
            echo (json_encode([
                'success' => false,
                'message' => $e->getMessage()
            ] ,JSON_UNESCAPED_UNICODE));
        }
    }

    public function insert(){
        header("Access-Control-Allow-Origin: ".ALLOWED_ACCESS);
        header('Content-Type: application/json; charset=UTF-8');
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type");

        try{
            $this->method('POST');
            //$this->authenticate();
            $data = $_POST;
            $this->mainModel->insert($data);
            echo (json_encode([
                'success' => true,
            ] ,JSON_UNESCAPED_UNICODE));
        } catch (Exception $e){
            echo (json_encode([
                'success' => false,
                'message' => $e->getMessage()
            ] ,JSON_UNESCAPED_UNICODE));
        }
    }
    public function update($id){
        header("Access-Control-Allow-Origin: ".ALLOWED_ACCESS);
        header('Content-Type: application/json; charset=UTF-8');
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type");

        try{
            $this->method('POST');
            //$this->authenticate();

            $data = $_POST;
            $this->mainModel->update($id,$data);
            echo (json_encode([
                'success' => true,
            ] ,JSON_UNESCAPED_UNICODE));
        } catch (Exception $e){
            echo (json_encode([
                'success' => false,
                'message' => $e->getMessage()
            ] ,JSON_UNESCAPED_UNICODE));
        }
    }
}