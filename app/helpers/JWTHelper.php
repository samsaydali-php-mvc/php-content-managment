<?php
/**
 * Created by PhpStorm.
 * User: Samer
 * Date: 8/26/2018
 * Time: 3:12 PM
 */
use Firebase\JWT\JWT;

class JWTHelper
{
    public static function sign($payload) {
        $payload['exp'] = time() + 60*60;

        // encode the payload using our Secret Key and return the token
        $token = JWT::encode($payload, AUTH_KEY);
        return $token;
    }

    // Auth header
    public static function valid($token){
        $token = explode(' ',$token);
        if(isset($token[1])){
            $token = $token[1];
            $decoded = JWT::decode($token, AUTH_KEY, array('HS256'));
            return (object) $decoded;
        } else {
            throw new Exception('Missing authorization header');
        }
    }

    // Decode string token
    public static function extract($token){
        $decoded = JWT::decode($token, AUTH_KEY, array('HS256'));
        return (object) $decoded;
    }
}