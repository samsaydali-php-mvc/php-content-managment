<?php
/**
 * Created by PhpStorm.
 * User: PC-02
 * Date: 9/2/2018
 * Time: 8:49 PM
 */

use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage; // Notice That there is a Mime in the location
use Zend\Mime\Part;
use Zend\Mail\Message; // Notice That there is a Mail in the location

class ZendMailer
{
    private $transport;
    public function __construct(){
        // Build Transport
        $this->transport = new Smtp();
        $options = new SmtpOptions([
            'host' => EMAIL_HOST,
            'connection_class' => 'login', // ?
            'port' => EMAIL_PORT,
            'connection_config' => [
                'username' => EMAIL,
                'password' => EMAIL_PASSWORD,
                'ssl' => 'tls',
                'auth' => 'login'
            ]
        ]);
        $this->transport->setOptions($options);
    }

    public function sendTestMail(){
        $part = new Part('<h1>Hello World!</h1><br><p>this is a test email</p>');
        $part->type = 'text/html';
        $body = new MimeMessage();
        $body->addPart($part);

        // Send Message
        $message = new Message();
        $message->setBody($body);
        $message->setFrom(EMAIL);
        $message->addTo('samsaydali@gmail.com');
        $message->setSubject('PHP Test Mail');

        // Send Message
        $this->transport->send($message);
    }

}