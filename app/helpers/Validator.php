<?php
/**
 * Created by PhpStorm.
 * User: PC-02
 * Date: 9/5/2018
 * Time: 8:41 PM
 */

class Validator
{
    public static function validate($filedName,$notValidError,$filterSanitize,$required,$requiredError = null,$filterValidate = null){

        if($filterValidate != null){
            return Validator::validateOther($filedName,$requiredError,$notValidError,$filterSanitize,$required,$filterValidate);
        } else {
            return Validator::validateString($filedName,$requiredError,$notValidError,$filterSanitize,$required);
        }
    }

    private function validateString($filedName,$requiredError,$notValidError,$filterSanitize,$required){
        $result = null;

        if( (isset($_POST[$filedName])) && (trim($_POST[$filedName]) != '') ){
            $result = filter_var(trim($_POST[$filedName],$filterSanitize));
            if($result == ''){
                throw new Exception($notValidError);
            }
        } else if($required){
            throw new Exception($requiredError);
        } else {
            $result = '';
        }

        return $result;

    }

    public function validateOther($filedName,$requiredError,$notValidError,$filterSanitize, $required ,$filterValidate){
        $result = null;

        if( (isset($_POST[$filedName])) && (trim($_POST[$filedName]) != '') ){
            $result = filter_var(trim($_POST[$filedName],$filterSanitize));
            if(!filter_var($result, $filterValidate)){
                throw new Exception($notValidError);
            }
        } else if($required){
            throw new Exception($requiredError);
        } else {
            $result = '';
        }

        return $result;
    }

    public static function notEmpty($filedName,$requiredError){
        $result = null;

        if( (isset($_POST[$filedName])) && (trim($_POST[$filedName]) != '') ){
            $result = trim($_POST[$filedName]);
            if($result == null || $result == 'null'){
                throw new Exception($requiredError);
            }
        } else {
            throw new Exception($requiredError);
        }

        return $result;
    }
}