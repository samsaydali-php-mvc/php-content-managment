<?php
/**
 * Class JSONHandler
 * Reads JSON files as arrays or object.
 * Saves objects or arrays to JSON files.
 */
class JSONHandler
{
    private $filePath;

    public function __construct($filePath){
        $this->filePath = $filePath;
    }

    /**
     * @param $filename string the file name.
     * @param bool $assoc true for returning associative array, false for object.
     * @return mixed  array | object
     */
    public function read($filename,$assoc = true){
        $filename = $this->filePath.'/'.$filename;
        $json_data = file_get_contents($filename);
        $array = json_decode($json_data,$assoc);
        return $array;
    }

    /**
     * @param $filename string the file name.
     * @param $data array|object data to save as JSON
     * @return void
     */
    public function write($filename,$data){
        $filename = $this->filePath.'/'.$filename;
        $json_data = json_encode($data);
        file_put_contents($filename, $json_data);
    }
}