<?php
    // Load Config
    require_once 'config/config.php';

    // Autoload Core Libraries
    spl_autoload_register(function($className){
        require_once 'libraries/' . $className . '.php';
    });

    // Load Helpers
    require_once 'helpers/AbstractModel.php';
    require_once 'helpers/AbstractContentController.php';
    require_once 'helpers/JWTHelper.php';
    require_once 'helpers/Validator.php';
    require_once 'helpers/Form.php';
    require_once 'helpers/JSONHandler.php';

    // Load Management Classes
    require_once 'managment/ContentTypesManger.php';
    require_once 'managment/RequestManager.php';
    require_once 'managment/CMController.php';




  
