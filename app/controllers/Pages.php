<?php


  class Pages extends Controller {
    public function __construct(){

    }
    
    public function index(){
      
      $data = [
        'title' => 'Welcome To PHP MVC',
        'description' => 'Core framework is done!, Focus on your app!'
      ];
     
      $this->view('pages/index', $data);
    }

    public function contact(){
        $category = new stdClass();
        $category->name = 'category';
        $category->icon = 'archive';
        $category->link = 'link';
        $contents = [$category];
        $data = new stdClass();
        $data->contents = $contents;


        $this->view('dashboard/inc/header',$data);
        echo "<head>
                <link href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">
                
                </head> <body><div class='container'>";
        Form::open('/test/input','post');
        Form::text('email','email','','this is only a help');
        Form::textarea('body','The Body',5);
        Form::select('animal',[1 => 'cat',2 => 'dog',3 => 'brush'],"2",'Pick an animal');
        Form::file('thumbnail');
        Form::radio('gender', ['male'=>'Male','female'=>'Female'] , 'female');
        Form::checkbox('agree_terms','true',false,'Agree Terms');
        Form::submit();
        Form::close();

        //$this->view('pages/about', $data);

        echo "</div></body>";

        $this->view('dashboard/inc/footer');
    }
  }