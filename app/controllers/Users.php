<?php
/**
 * Created by PhpStorm.
 * User: Samer
 * Date: 8/25/2018
 * Time: 10:08 PM
 */

require_once '../app/models/User.php';

class Users extends Controller
{
    private $usersModel;

    public function __construct(){
        $this->usersModel = new User();
    }

    private function register(){
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

        try{
            $this->method('POST');

            $password = $_POST['password'];
            $email = $_POST['email'];
            $username = $_POST['username'];

            $email = filter_var($email,FILTER_SANITIZE_EMAIL);
            $username = filter_var($username,FILTER_SANITIZE_STRING);

            if(filter_var($email,FILTER_VALIDATE_EMAIL)) {

                $this->usersModel->register($username, $password, $email);
                echo json_encode(array(
                    'success' => true,
                    'message' => 'User created successfully!'
                ));
            } else {
                throw new Exception('Email is not valid');
            }
        } catch (Exception $e){
            echo json_encode(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }

    }

    public function login(){
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

        try{
            $this->method('POST');

            $password = $_POST['password'];
            $username = $_POST['username'];

            $username = filter_var($username,FILTER_SANITIZE_STRING);

            $user = $this->usersModel->login($username,$password);
            echo json_encode(array(
                'success' => true,
                'message' => 'User signed successfully!',
                'user' => $user
            ));
        } catch (Exception $e){
            echo json_encode(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }

    public function changePassword(){
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

        try{
            $this->method('POST');
            $this->authenticate();

            $oldPassword = Validator::notEmpty('oldPassword','Old Password Required');
            $newPassword = Validator::notEmpty('newPassword','New Password Required');
            $username = Validator::validate('username','Username not valid!',FILTER_SANITIZE_STRING,true,'Username is required');

            $this->usersModel->changePassword($username,$oldPassword,$newPassword);

            echo json_encode(array(
                'success' => true,
                'message' => 'Password changed successfully'
            ));

        } catch (Exception $e){
            echo json_encode(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }

    public function forgetPassword(){
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

            try{
                $this->method('POST');
                $username = Validator::validate('username','Username not valid!',FILTER_SANITIZE_STRING,true,'Username is required');
                $email = Validator::validate('email','Email not valid!',FILTER_VALIDATE_EMAIL,true,'Email is required',FILTER_VALIDATE_EMAIL);

                $payload = $this->usersModel->forgetPassword($username,$email);

                $mailer = new Mailer();
                $mailer->SendNewPasswordMail($payload['newPassword'],URLROOT.'/users/confirmForgetPassword/'.$payload['id']);

                echo json_encode(array(
                    'success' => true,
                    'message' => 'Password forget request made successfully, Please check email!'
                ));

            } catch (Exception $e){
                echo json_encode(array(
                    'success' => false,
                    'message' => $e->getMessage()
                ));
            }
    }

    public function confirmForgetPassword($id){
        try{
            $user = JWTHelper::extract($id);
            $this->usersModel->confirmForgetPassword($user->username,$user->newPassword);
            echo 'Password reset successfully!';

        } catch (Exception $e){
            echo $e->getMessage();
        }
    }

    public function valid(){
        die($_SERVER['HTTP_AUTHORIZATION']);
    }
}